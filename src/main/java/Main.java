import org.apache.commons.lang3.RandomUtils;

public class Main {
    public static void main(String[] args) {
        System.out.println(generateWelcomPhase());
        System.out.println(generateInt());
        System.out.println(generateFloat());
    }
    public static float generateFloat(){
        return RandomUtils.nextFloat();
    }

public static int generateInt(){
        return RandomUtils.nextInt();
}
private static String generateWelcomPhase(){
        return "Phase_1";
}
}
