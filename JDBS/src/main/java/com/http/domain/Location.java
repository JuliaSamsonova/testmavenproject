package com.http.domain;

import java.util.Objects;

public class Location {
    private int id;
    private int carId;
    private String carRegion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getCarRegion() {
        return carRegion;
    }

    public void setCarRegion(String carRegion) {
        this.carRegion = carRegion;
    }

    public Location() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return id == location.id &&
                carId == location.carId &&
                Objects.equals(carRegion, location.carRegion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, carId, carRegion);
    }

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", carId=" + carId +
                ", carRegion='" + carRegion + '\'' +
                '}';
    }
}


