package com.http.dao;

import com.http.domain.Car;
import com.http.domain.Location;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface LocationDao {
    List<Location> findAll() throws SQLException;

    List<Location> search(String searchParam);

    Optional<Location> findById(int locationId);

    Location findOne(int locationId); //при вызове данного метода user должен быть (try-catch)

    Location save(Location location);

    Location update(Location location);

    int delete(Location location);
}

