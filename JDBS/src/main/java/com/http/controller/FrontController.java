package com.http.controller;

import com.http.dao.*;
import com.http.domain.Car;
import com.http.domain.Location;
import com.http.domain.User;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

public class FrontController extends HttpServlet {
    public static final String FIND_ONE = "findOne";
    public static final String FIND_BY_ID = "findById";
    public static final String FIND_ALL = "findAll";
    public static final String SAVE = "save";
    public static final String UPDATE = "update";
    public static final String DELETE = "delete";

    private UserDao userDao = new UserDaoImpl();
    private CarDao carDao = new CarDaoImpl();
    private LocationDao locationDao = new LocationDaoImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doRequest(req, resp);
    }

    private void doRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userSearchQuery = StringUtils.isNotBlank(req.getParameter("userId")) ? req.getParameter("userId") : "0";
        String carSearchQuery = StringUtils.isNotBlank(req.getParameter("carId")) ? req.getParameter("carId") : "0";
        String locationSearchQuery = StringUtils.isNotBlank(req.getParameter("locationId"))?req.getParameter("locationId"): "0";
        if (userSearchQuery != "0") {
            doUser(req, userSearchQuery, resp);
        } else if (carSearchQuery != "0") {
            doCar(req, carSearchQuery, resp);
        } else if (locationSearchQuery!="0"){
            doLocation(req, locationSearchQuery,resp);
        }  else {
            throw new RuntimeException("Exception");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doRequest(req, resp);
    }

    private void doUser(HttpServletRequest req, String userSearchQuery, HttpServletResponse resp) throws ServletException, IOException {
        String typeOfSearch = StringUtils.isNotBlank(req.getParameter("type")) ? req.getParameter("type") : "0";
        String userName = StringUtils.isNotBlank(req.getParameter("userName")) ? req.getParameter("userName") : "0";
        String userSurnameName = StringUtils.isNotBlank(req.getParameter("userSurname")) ? req.getParameter("userSurname") : "0";
        try {
            RequestDispatcher dispatcher = req.getRequestDispatcher("/bye");
            if (dispatcher != null) {
                String result = "";


                switch (typeOfSearch) {
                    case FIND_ONE:
                        result = userDao.findOne(Integer.parseInt(userSearchQuery)).getLogin();
                        break;
                    case FIND_BY_ID:
                        Optional<User> optionalUser = userDao.findById(Integer.parseInt(userSearchQuery));
                        if (optionalUser.isPresent()) {
                            result = optionalUser.get().getLogin();
                        }
                        break;

                    case SAVE:
                        User user = new User();
                        user.setUsername(userName);
                        user.setSurname(userSurnameName);
                        user.setBirthDate(new java.sql.Date(new Date().getTime()));
                        user.setLogin(UUID.randomUUID().toString());
                        user.setPassword(UUID.randomUUID().toString());
                        user.setCreated(new Timestamp(new Date().getTime()));
                        user.setChanged(new Timestamp(new Date().getTime()));
                        result = userDao.save(user).getLogin();

                    case UPDATE:
                        User userForUpdate = userDao.findOne(Integer.parseInt(userSearchQuery));
                        userForUpdate.setUsername(userName);
                        userForUpdate.setSurname(userSurnameName);
                        userForUpdate.setBirthDate(new java.sql.Date(new Date().getTime()));
                        userForUpdate.setPassword(UUID.randomUUID().toString());
                        userForUpdate.setLogin(UUID.randomUUID().toString());
                        userForUpdate.setChanged(new Timestamp(new Date().getTime()));
                        userForUpdate.setCreated(new Timestamp(new Date().getTime()));
                        result = userDao.update(userForUpdate).getLogin();

                        break;

                    case DELETE:
                        User userForDelete = userDao.findOne(Integer.parseInt(userSearchQuery));
                        userForDelete.setUsername(userName);
                        userForDelete.setSurname(userSurnameName);
                        userForDelete.setBirthDate(new java.sql.Date(new Date().getTime()));
                        userForDelete.setPassword(UUID.randomUUID().toString());
                        userForDelete.setLogin(UUID.randomUUID().toString());
                        userForDelete.setChanged(new Timestamp(new Date().getTime()));
                        userForDelete.setCreated(new Timestamp(new Date().getTime()));
                        result = "DELETE" + userDao.update(userForDelete);

                        break;

                    case FIND_ALL:
                    default:
                        result = userDao.findAll().stream().map(User::getLogin).collect(Collectors.joining(","));
                        break;
                }
                req.setAttribute("userNames", result);
                req.setAttribute("userSurname", result);
                dispatcher.forward(req, resp);
            }
        } catch (Exception e) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("/error");
            req.setAttribute("errors", e.getMessage());
            dispatcher.forward(req, resp);
        }
    }

    private void doCar(HttpServletRequest req, String carSearchQuery, HttpServletResponse resp) throws ServletException, IOException {
        String typeOfSearch = StringUtils.isNotBlank(req.getParameter("type")) ? req.getParameter("type") : "0";
        String model = StringUtils.isNotBlank(req.getParameter("model")) ? req.getParameter("model") : "0";

        try {
            RequestDispatcher dispatcher = req.getRequestDispatcher("/hello");
            if (dispatcher != null) {
                String result = "";

                switch (typeOfSearch) {
                    case FIND_ONE:
                        result = carDao.findOne(Integer.parseInt(carSearchQuery)).getModel();
                        break;

                    case FIND_BY_ID:
                        Optional<Car> optionalCar = carDao.findById(Integer.parseInt(carSearchQuery));
                        if (optionalCar.isPresent()) {
                            result = optionalCar.get().getModel();
                        }
                        break;

                    case SAVE:
                        Car car = new Car();
                        car.setModel(model);
                        car.setPrice(new Random().nextInt());
                        car.setNumber_of_cars(new Random().nextInt());
                        car.setGuarantee_expiration_date(new Timestamp(new Date().getTime()));
                        result = carDao.save(car).getModel();
                        break;

                    case UPDATE:
                        Car carForUpdate = carDao.findOne(Integer.parseInt(carSearchQuery));
                        carForUpdate.setModel(model);
                        carForUpdate.setPrice(new Random().nextInt());
                        carForUpdate.setNumber_of_cars(new Random().nextInt());
                        carForUpdate.setGuarantee_expiration_date(new Timestamp(new Date().getTime()));
                        result = carDao.update(carForUpdate).getModel();
                        break;

                    case DELETE:
                        Car carForDelete = carDao.findOne(Integer.parseInt(carSearchQuery));
                        carForDelete.setModel(model);
                        carForDelete.setPrice(new Random().nextInt());
                        carForDelete.setNumber_of_cars(new Random().nextInt());
                        carForDelete.setGuarantee_expiration_date(new Timestamp(new Date().getTime()));
                        result = "Delete"+ carDao.delete(carForDelete);
                        break;

                    case FIND_ALL:
                    default:
                        result = carDao.findAll().stream().map(Car::getModel).collect(Collectors.joining(","));
                        break;
                }
                req.setAttribute("models", result);
                dispatcher.forward(req, resp);
            }
        } catch (Exception e) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("/error");
            req.setAttribute("errors", e.getMessage());
            dispatcher.forward(req, resp);
        }
    }
    private void doLocation(HttpServletRequest req, String locationSearchQuery, HttpServletResponse resp) throws ServletException, IOException {
        String typeOfSearch = StringUtils.isNotBlank(req.getParameter("type")) ? req.getParameter("type") : "0";
        String carRegion = StringUtils.isNotBlank(req.getParameter("carRegion")) ? req.getParameter("carRegion") : "0";

        try {
            RequestDispatcher dispatcher = req.getRequestDispatcher("/location");
            if (dispatcher != null) {
                String result = "";


                switch (typeOfSearch) {
                    case FIND_ONE:
                        result = locationDao.findOne(Integer.parseInt(locationSearchQuery)).getCarRegion();
                        break;
                    case FIND_BY_ID:
                        Optional<Location> optionalLocation = locationDao.findById(Integer.parseInt(locationSearchQuery));
                        if (optionalLocation.isPresent()) {
                            result = optionalLocation.get().getCarRegion();
                        }
                        break;

                    case SAVE:
                        Location location = new Location();
                        location.setId(new Random().nextInt());
                        location.setCarId(new Random().nextInt());
                        location.setCarRegion(carRegion);

                        result = locationDao.save(location).getCarRegion();

                    case UPDATE:
                        Location locationForUpdate = locationDao.findOne(Integer.parseInt(locationSearchQuery));
                        locationForUpdate.setId(new Random().nextInt());
                        locationForUpdate.setCarId(new Random().nextInt());
                        locationForUpdate.setCarRegion(carRegion);

                        result = locationDao.update(locationForUpdate).getCarRegion();

                        break;

                    case DELETE:
                        Location locationForDelete = locationDao.findOne(Integer.parseInt(locationSearchQuery));
                        locationForDelete.setId(new Random().nextInt());
                        locationForDelete.setCarId(new Random().nextInt());
                        locationForDelete.setCarRegion(carRegion);

                        result = "DELETE" + locationDao.update(locationForDelete);

                        break;

                    case FIND_ALL:
                    default:
                        result = locationDao.findAll().stream().map(Location::getCarRegion).collect(Collectors.joining(","));
                        break;
                }
                req.setAttribute("carRegion", result);
                dispatcher.forward(req, resp);
            }
        } catch (Exception e) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("/error");
            req.setAttribute("errors", e.getMessage());
            dispatcher.forward(req, resp);
        }
    }
}










