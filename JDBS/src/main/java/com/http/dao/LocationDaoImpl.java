package com.http.dao;

import com.http.domain.Location;
import com.http.exceptions.ResourceNotFoundException;
import com.http.util.DatabaseConfiguration;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LocationDaoImpl implements LocationDao {
    public static DatabaseConfiguration config = DatabaseConfiguration.getInstance();
    public static final String LOCATION_ID = "id";
    public static final String LOCATION_CAR_ID = "car_id";
    public static final String LOCATION_CAR_REGION = "carRegion";

    @Override
    public List<Location> findAll() throws SQLException {
        final String findAllQuery = "select * from l_car_location order by id desc";
        String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
        String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
        String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);
        String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getException());
        }
        List<Location> resultList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, password, login)) {
            PreparedStatement preparedStatement = connection.prepareStatement(findAllQuery);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                resultList.add(parseResultSet(resultSet));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return resultList;
    }

    @Override
    public List<Location> search(String searchParam) {
        final String searchQuery = "select location_id as l_car_location_id, location_car_id as l_car_location_car_id  from l_car_location,  where id=? order by id desc";
        String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
        String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
        String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);
        String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getException());
        }
        List<Location> resultList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, password, login)) {
            PreparedStatement preparedStatement = connection.prepareStatement(searchQuery);
            preparedStatement.setInt(1, Integer.parseInt(searchParam));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                resultList.add(parseResultSet(resultSet));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return resultList;
    }

    @Override
    public Optional<Location> findById(int locationId) {
        return Optional.ofNullable(findOne(locationId));
    }

    @Override
    public Location findOne(int locationId) {
        final String findById = "select * from l_car_location where id=?";
        String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
        String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
        String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
        String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        List<Location> resultList = new ArrayList<>();
        Location location = null;
        ResultSet resultSet = null;
        try (Connection connection = DriverManager.getConnection(url, login, password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(findById);
            preparedStatement.setInt(1, locationId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                location = parseResultSet(resultSet);
            } else {
                throw new ResourceNotFoundException("Location with id" + locationId + "not found");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                resultSet.close();
            } catch (SQLException throwables) {
                System.out.println(throwables.getMessage());
            }
        }
        return location;
    }


    @Override
    public Location save(Location location) {
        final String insertQuery = "INSERT INTO public.l_car_location (id, car_id, car_region)\n" +
                "VALUES ( ?, ?, ?)";
        String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
        String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
        String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
        String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try (Connection connection = DriverManager.getConnection(url, login, password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
            PreparedStatement lastInsertId = connection.prepareStatement("select currval ('l_car_location id_seq')as last_inset_id;");
            preparedStatement.setInt(1, location.getId());
            preparedStatement.setInt(2, location.getCarId());
            preparedStatement.setString(3, location.getCarRegion());

            preparedStatement.executeUpdate();

            ResultSet set = lastInsertId.executeQuery();
            set.next();
            int locationId = set.getInt("last_insert_id");
            return findOne(locationId);
        } catch (SQLException e) {
            throw new RuntimeException("Some issues in insert operation!", e);
        }
    }

    @Override
    public Location update(Location location) {
        final String updateQuery = "UPDATE public.l_car_location (id=?,car_id=?,car_region=?)\n" +
                "VALUES ( ?, ?, ?)";
        String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
        String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
        String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
        String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try (Connection connection = DriverManager.getConnection(url, login, password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(updateQuery);
            PreparedStatement lastInsertId = connection.prepareStatement("select currval ('l_car_location id_seq')as last_inset_id;");
            preparedStatement.setInt(1, location.getId());
            preparedStatement.setInt(2, location.getCarId());
            preparedStatement.setString(2, location.getCarRegion());

            preparedStatement.executeUpdate();

            ResultSet set = lastInsertId.executeQuery();
            set.next();
            int locationId = set.getInt("last_insert_id");
            return findOne(locationId);
        } catch (SQLException e) {
            throw new RuntimeException("Some issues in insert operation!", e);
        }
    }

    @Override
    public int delete(Location location) {
        final String locationForDelete = "DELETE public.l_car_location (id=?,car_id=?,car_region=?)\n" +
                "WHERE l_car_location_id = ?";
        String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
        String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
        String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
        String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);

        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try (Connection connection = DriverManager.getConnection(url, login, password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(locationForDelete);
            PreparedStatement lastInsertId = connection.prepareStatement("select currval ('l_car_location id_seq')as last_inset_id;");
            preparedStatement.setInt(1, location.getId());
            preparedStatement.setInt(2, location.getCarId());
            preparedStatement.setString(3, location.getCarRegion());
            preparedStatement.executeUpdate();

            ResultSet set = lastInsertId.executeQuery();
            set.next();

            int locationId = set.getInt("last inserted id");
            return locationId;
        } catch (SQLException e) {
            throw new RuntimeException("Some issues in insert operation!", e);
        }
    }

    private Location parseResultSet(ResultSet resultSet) throws SQLException {
        Location location = new Location();
        location.setId(resultSet.getInt(LOCATION_ID));
        location.setCarId(resultSet.getInt(LOCATION_CAR_ID));
        location.setCarRegion(resultSet.getString(LOCATION_CAR_REGION));
        return location;
    }
}






