package dao;
import com.http.domain.User;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface UserDao {
    /*CRUD operations*/
    /*Create = Insert*/
    /*Read = Select*/
    /*Update*/
    /*Delete*/
    List<User> findAll() throws SQLException;

    List<User> search (String searchParam);

    Optional<User>findById (int userId);

    User findOne(int userId); //при вызове данного метода user должен быть (try-catch)

    User save (User user);

    User update (User user);

    int delete (User user);


    }

