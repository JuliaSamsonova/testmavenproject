//package com.http.aspect;
//
//
//import org.apache.log4j.Logger;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Pointcut;
//import org.springframework.stereotype.Component;
//
//@Aspect
//@Component
//public class LogAspect {
//    private static final Logger log = Logger.getLogger("LogAspect.class");
//
//    @Pointcut("execution(* com.http.dao.jdbctemplate.UserRepository.*(..))")
//    public void aroundRepositoryPointcut() {
//    }
//
//    @Around("aroundRepositoryPointcut()")
//    public Object logAroundMethods(ProceedingJoinPoint joinPoint) throws Throwable {
//        log.info("Method " + joinPoint.getSignature().getName() + " start");
//        Object proceed = joinPoint.proceed();
//        log.info("Method " + joinPoint.getSignature().getName() + " finished");
//        return proceed;
//    }
//}
