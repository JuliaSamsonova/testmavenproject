package com.http.service;

import com.http.domain.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

public class SpringDemo {
    private static final Logger log = Logger.getLogger("SpringDemo.class");

    public static void main(String[] args) throws SQLException {
        ApplicationContext context = new AnnotationConfigApplicationContext("com.http");
        UserService userService = (UserService) context.getBean("userServiceImpl");
        List<User> allUsers = userService.findAll();
        User allUsers2 = userService.findOne(1);
        Optional<User> optionalUser = userService.findById(1);
        String serachParam = "1";
        List<User> allUsers3 = userService.search(serachParam);
        for (User user : allUsers3) {
            log.info(user.getLogin());
        }
        /*for save method*/
        Date birthDayDate = generateBirthDate();
        Timestamp changedData = (Timestamp) generateChangedData();
        Timestamp createdData = (Timestamp) generateCreatedData();
        User user1 = new User(13, "David", "Cockborn", birthDayDate, "DaveC12", "123456", createdData, changedData, false, 52, 28);
        User allUsers4 = userService.save(user1);
        /*for update method*/
        User user2 = new User(13, "David", "Cockborn", birthDayDate, "DaveCoc1", "123456", createdData, changedData, false, 52, 28);
        User allUsers5 = userService.update(user2);
        /*for delete method*/
        User user12 = new User();
        int allUsers6 = userService.delete(user12);
    }

    private static Date generateCreatedData() {
        Timestamp createdData1 = new Timestamp(System.currentTimeMillis());
        Date date = new Date(createdData1.getTime());
        System.out.println(date);
        return createdData1;
    }

    private static Date generateChangedData() {
        Timestamp changedData1 = new Timestamp(System.currentTimeMillis());
        Date date = new Date(changedData1.getTime());
        System.out.println(date);
        return changedData1;
    }


    private static Date generateBirthDate() {
        Date birthDayDate1 = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String birthDayDateString = "02-07-2020 11:35:42";
        try {
            birthDayDate1 = dateformat.parse(birthDayDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return birthDayDate1;
    }
}
