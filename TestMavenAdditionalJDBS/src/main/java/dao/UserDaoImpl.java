package dao;

import com.http.domain.User;
import com.http.exceptions.ResourceNotFoundException;
import com.http.util.DatabaseConfiguration;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public  class UserDaoImpl implements UserDao{
    public  static DatabaseConfiguration config = DatabaseConfiguration.getInstance();
    public  static final String USER_ID = "id";
    public static final String USER_NAME = "userName";
    public static final String USER_SURNAME = "surname";
    public static final String USER_BIRTH_DATE = "birth_date";
    public  static final String USER_LOGIN = "login";
    public static final String USER_PASSWORD = "password";
    public static final String USER_CREATED = "created";
    public  static final String USER_CHANGED = "changed";
    public  static final String USER_IS_BLOCKED = "is_blocked";
    public  static final String USER_WEIGHT = "weight";

    @Override
    public List<User> findAll() {
        final String findAllQuery = "select from m_users order by id desc";
        String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
        String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
        String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);
        String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
        try{
            Class.forName(driverName);
        }catch (ClassNotFoundException e){
            System.out.println(e.getException());
        }
        List<User>resultList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, password, login)){
        PreparedStatement preparedStatement = connection.prepareStatement(findAllQuery);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()){
        resultList.add(parseResultSet(resultSet));
        }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return resultList;
    }

    @Override
    public List<User> search(String searchParam) {
        final String findAllQueryForPrepared = "select* from m_users where id>? order by id desc";
        String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
        String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
        String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);
        String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
        try {
            Class.forName(driverName);
        }catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        List<User>resultList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, login, password)){
        PreparedStatement preparedStatement = connection.prepareStatement(findAllQueryForPrepared);
        preparedStatement.setLong(1, Long.parseLong(searchParam));
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()){
        resultList.add(parseResultSet(resultSet));
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultList;
    }

    @Override
    public Optional<User> findById(int userId) {
        return Optional.ofNullable(findOne(userId));
    }

    @Override
    public User findOne(int userId) {
        final String findById = "select* from m_users where id=?";
        String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
        String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
        String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
        String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);
        try {
            Class.forName(driverName);
        }catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
             }
        List<User>resultList = new ArrayList<>();
        User user = null;
        ResultSet resultSet = null;
        try (Connection connection = DriverManager.getConnection(url, login, password)) {
        PreparedStatement preparedStatement = connection.prepareStatement(findById);
        preparedStatement.setLong(1, userId);
        resultSet = preparedStatement.executeQuery();
        if (resultSet.next()){
        user = parseResultSet(resultSet);
        } else {
            throw new ResourceNotFoundException("User with id" + userId + "not found");
        }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            try{
                resultSet.close();
            }catch (SQLException throwables){
                System.out.println(throwables.getMessage());
            }
        }

        return user;
    }

    @Override
    public User save(User user) {
        final String insertQuery = "INSERT INTO public.m_users (username, surname, birth_date, login, password, created, changed, is_blocked)\n" +
                "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?)";
        String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
        String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
        String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
        String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);
        try {
            Class.forName(driverName);
        }catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        try (Connection connection = DriverManager.getConnection(url, login, password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
            PreparedStatement lastInsertId = connection.prepareStatement("select currval ('m_users id_seq')as last_inset_id;");
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setDate(3, (Date) user.getBirthDate());
            preparedStatement.setString(4, user.getLogin());
            preparedStatement.setString(5, user.getPassword());
            preparedStatement.setTimestamp(6, user.getCreated());
            preparedStatement.setTimestamp(7, user.getChanged());
            preparedStatement.setBoolean(8, user.isBlocked());

            preparedStatement.executeUpdate();

            ResultSet set = lastInsertId.executeQuery();
            set.next();
            int insertedUserId = set.getInt("last_insert_id");
            return findOne(insertedUserId);
        }catch (SQLException e){
            throw new RuntimeException("Some issues in insert operation!", e);
        }
        }

        @Override
    public User update(User user) {
            final String updateQuery = "UPDATE public.m_users set username=?, surname=?, birth_date=?, login=?, password=?, created=?, changed=?, is_blocked=?)\n" +
                    "WHERE id= ?";
            String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
            String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
            String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
            String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);
            try {
                Class.forName(driverName);
            }catch (ClassNotFoundException e) {
                System.out.println(e.getMessage());
            }
                try (Connection connection = DriverManager.getConnection(url, login, password)) {
                    PreparedStatement preparedStatement = connection.prepareStatement(updateQuery);
                    PreparedStatement lastInsertId = connection.prepareStatement("select currval ('m_users id_seq')as last_inset_id;");
                    preparedStatement.setString(1, user.getUsername());
                    preparedStatement.setString(2, user.getSurname());
                    preparedStatement.setDate(3, (Date) user.getBirthDate());
                    preparedStatement.setString(4, user.getLogin());
                    preparedStatement.setString(5, user.getPassword());
                    preparedStatement.setTimestamp(6, user.getCreated());
                    preparedStatement.setTimestamp(7, user.getChanged());
                    preparedStatement.setBoolean(8, user.isBlocked());

                    preparedStatement.setInt(9,user.getId());

                    preparedStatement.executeUpdate();

                    return  findOne(user.getId());
                }catch (SQLException e1){
                        throw new RuntimeException("Some issues in insert operation!", e1);
            }
           }

    @Override
    public int delete(User user) {
        return 0;
    }


    private User parseResultSet(ResultSet resultSet) throws SQLException {
    User user = new User();
    user.setUsername(resultSet.getString(USER_NAME));
    user.setSurname(resultSet.getString(USER_SURNAME));
    user.setId(resultSet.getInt(USER_ID));
    user.setBirthDate(resultSet.getDate(USER_BIRTH_DATE));
    user.setLogin(resultSet.getString(USER_LOGIN));
    user.setPassword(resultSet.getString(USER_PASSWORD));
    user.setCreated(resultSet.getTimestamp(USER_CREATED));
    user.setChanged(resultSet.getTimestamp(USER_CHANGED));
    user.setBlocked(resultSet.getBoolean(USER_IS_BLOCKED));
    user.setWeight(resultSet.getFloat(USER_WEIGHT));
    return user;
    }
}


















