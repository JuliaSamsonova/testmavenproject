package com.http.service;

import com.http.dao.UserDao;
import com.http.domain.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private UserDao userDao;

    public UserServiceImpl(@Qualifier("userRepositoryJdbcTemplate") UserDao userDao) {
        this.userDao = userDao;
    }

    public User findOne(Integer userId) {
        User allUsers2 = userDao.findOne(userId);
        return allUsers2;
    }

    public List<User> findAll() throws SQLException {
        List<User> allUsers = userDao.findAll();
        return allUsers;
    }

    public Optional<User> findById(Integer userId) {
        Optional<User> optionalUser = userDao.findById(userId);
        return optionalUser;
    }

    public List<User> search(String searchParamString) {
        int searchParam = Integer.parseInt(searchParamString);
        List<User> allUsers3 = userDao.search(searchParam);
        return allUsers3;
    }

    @Override
    public User save(User user) {
        User allUsers4 = userDao.save(user);
        return allUsers4;
    }

    @Override
    public User update(User user) {
        User allUsers5 = userDao.update(user);
        return allUsers5;
    }

    @Override
    public int delete(User user) {
        int allUsers6 = userDao.delete(user);
        return allUsers6;
    }
}
