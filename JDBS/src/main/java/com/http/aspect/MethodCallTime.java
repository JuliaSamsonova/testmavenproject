package com.http.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.util.logging.Logger;

@Aspect
@Component
public class MethodCallTime {
    private static final Logger log = Logger.getLogger("MethodCallTime.class");

    @Pointcut("execution(* com.http.dao.*.*(..))")
    public void aroundRepositoryPointcut() {
    }

    @Around("aroundRepositoryPointcut()")
    public Object callTimeAroundMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.nanoTime();
        Object proceed = joinPoint.proceed();
        long stopTime = System.nanoTime();
        long resultTime = stopTime - startTime;
        log.info("Method execute " + joinPoint.getSignature().getName() + " time: " + resultTime + " nano seconds");
        return proceed;
    }
}
