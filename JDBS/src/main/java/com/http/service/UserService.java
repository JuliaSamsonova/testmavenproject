package com.http.service;

import com.http.domain.User;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> findAll() throws SQLException;

    List<User> search(String searchParamString);

    Optional<User> findById(Integer userId);

    User findOne(Integer userId);

    User save(User user);

    User update(User user);

    int delete(User user);
}

