package com.http.dao.jdbctemplate;

import com.http.dao.UserDao;
import com.http.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository("userRepositoryJdbcTemplate")
public class UserRepository implements UserDao {
    public static final String USER_ID = "id";
    public static final String USER_USERNAME = "username";
    public static final String USER_SURNAME = "surname";
    public static final String USER_BIRTH_DATE = "birth_date";
    public static final String USER_LOGIN = "login";
    public static final String USER_PASSWORD = "password";
    public static final String USER_CREATED = "created";
    public static final String USER_CHANGED = "changed";
    public static final String USER_IS_BLOCKED = "is_blocked";
    public static final String USER_WEIGHT = "weight";

    @Autowired
    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UserRepository(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public List<User> findAll() {
        final String findAllQuery = "select * from m_users order by id desc";
        List<User> result = jdbcTemplate.query(findAllQuery, this::userRowMapper);
        return result;
    }

    private User userRowMapper(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt(USER_ID));
        user.setUsername(resultSet.getString(USER_USERNAME));
        user.setSurname(resultSet.getString(USER_SURNAME));
        user.setBirthDate(resultSet.getDate(USER_BIRTH_DATE));
        user.setLogin(resultSet.getString(USER_LOGIN));
        user.setPassword(resultSet.getString(USER_PASSWORD));
        user.setCreated(resultSet.getTimestamp(USER_CREATED));
        user.setChanged(resultSet.getTimestamp(USER_CHANGED));
        user.setBlocked(resultSet.getBoolean(USER_IS_BLOCKED));
        user.setWeight(resultSet.getFloat(USER_WEIGHT));
        return user;
    }

    @Override
    public List<User> search(Integer searchParam) {
        final String searchQuery = "select * from m_users where id=? order by id desc";
        return jdbcTemplate.query(searchQuery, this::userRowMapper, searchParam);
    }

    @Override
    public Optional<User> findById(int userId) {
//        final String findOne = "select * from m_users where id = ?"/;
//        return Optional.of(jdbcTemplate.query(findOne(userId), this::userRowMapper,));
        return Optional.ofNullable(findOne(userId));
    }

    @Override
    public User findOne(int userId) {
        final String findById = "select * from m_users where id = ?";
        return jdbcTemplate.queryForObject(findById, this::userRowMapper, userId);
    }

    @Override
    public User save(User user) {
        String sql = "INSERT INTO m_users (id,username, surname, birth_date, login, password, created, changed, is_blocked, weight, temp) " +
                "VALUES ( ((SELECT max(id) FROM m_users) + 1) ,?,?,?,?,?,?,?,?,?,?)";
        String username = user.getUsername();
        String surname = user.getSurname();
        Date birthDate = user.getBirthDate();
        String login = user.getLogin();
        String password = user.getPassword();
        Timestamp created = user.getCreated();
        Timestamp changed = user.getChanged();
        boolean is_blocked = user.isBlocked();
        Float weight = user.getWeight();
        int temp = user.getTemp();
        jdbcTemplate.update(sql, new Object[]{username, surname, birthDate, login, password, created, changed, is_blocked, weight, temp});
        return null;
    }

    @Override
    public User update(User user) {
        String sql = "INSERT INTO m_users (id, username, surname, birth_date, login, password, created, changed, is_blocked, weight, temp) " +
                "VALUES (((SELECT max(id) FROM m_users) + 1), ?,?,?,?,?,?,?,?,?,?)";

        String username = user.getUsername();
        String surname = user.getSurname();
        Date birthDate = user.getBirthDate();
        String login = user.getLogin();
        String password = user.getPassword();
        Timestamp created = user.getCreated();
        Timestamp changed = user.getChanged();
        boolean is_blocked = user.isBlocked();
        Float weight = user.getWeight();
        int temp = user.getTemp();
        int result = jdbcTemplate.update(sql, new Object[]{username, surname, birthDate, login, password, created, changed, is_blocked, weight, temp});
        if (result != 0) {
            System.out.println("User data updated for ID " + user.getId());
        } else {
            System.out.println("No User found with ID " + user.getId());
        }
        return user;
    }

    @Override
    public int delete(User user) {
        String sql = "DELETE FROM public.m_users WHERE id= 12";
        int result = jdbcTemplate.update(sql);
        if (result != 0) {
            System.out.println("User data deleted for ID " + user.getId());
        } else {
            System.out.println("No User found with ID " + user.getId());
        }
        return 0;
    }
}



