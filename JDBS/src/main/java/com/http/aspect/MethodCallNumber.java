package com.http.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Component
@Aspect
public class MethodCallNumber {
    private static final Logger log = Logger.getLogger("MethodCallNumber.class");
    private static Map<String, Integer> number = new ConcurrentHashMap<>();

    public static String showCalledMethods() {
        return number.entrySet().stream().map(e -> e.getKey() + " " + e.getValue()).collect(Collectors.joining(","));
    }

    @Pointcut("execution(* com.http.dao.*.*(..))")
    public void aroundRepositoryPointcut() {
    }

    @Around("aroundRepositoryPointcut()")
    public Object callNumberAroundMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        number.putIfAbsent(joinPoint.getSignature().getName(), 0);
        Object proceed = joinPoint.proceed();
        number.computeIfPresent(joinPoint.getSignature().getName(), (k, v) -> v + 1);
        log.info("Number of called methods" + number);
        return proceed;
    }
}


