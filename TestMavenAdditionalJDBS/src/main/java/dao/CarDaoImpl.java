package dao;

import com.http.domain.Car;
import com.http.domain.User;
import com.http.exceptions.ResourceNotFoundException;
import com.http.util.DatabaseConfiguration;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CarDaoImpl implements CarDao{
    public  static DatabaseConfiguration config = DatabaseConfiguration.getInstance();
    public static final String CAR_ID = "id";
    public static final String CAR_MODEL = "carModel";
    public static final String CAR_GUARANTEE_EXPIRATION_DATE = "guarantee_expiration_date";
    public  static  final String CAR_PRICE = "price";
    public  static final String CAR_DEALER_ID = "dealer_id";
    public  static final String CAR_USER_ID = "user_id";
    public static final String CAR_NUMBER_OF_CARS = "number_of_cars";


    @Override
    public List<Car> findAll() throws SQLException {
        final String findAllQuery = "select from m_cars order by id desc";
        String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
        String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
        String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);
        String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
        try{
            Class.forName(driverName);
        }catch (ClassNotFoundException e){
            System.out.println(e.getException());
        }
        List<Car>resultList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, password, login)){
            PreparedStatement preparedStatement = connection.prepareStatement(findAllQuery);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                resultList.add(parseResultSet(resultSet));
            }
        }catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return resultList;
    }

    @Override
    public List<Car> search(String searchParam) {
        final String findAllQueryForPrepared = "select* from m_cars where id>? order by id desc";
        String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
        String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
        String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);
        String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
        try {
            Class.forName(driverName);
        }catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        List<Car>resultList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, login, password)){
            PreparedStatement preparedStatement = connection.prepareStatement(findAllQueryForPrepared);
            preparedStatement.setLong(1, Long.parseLong(searchParam));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                resultList.add(parseResultSet(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultList;
    }

    @Override
    public Optional<Car> findById(int carId) {
        return Optional.ofNullable(findOne(carId));
    }

    @Override
    public Car findOne(int carId) {
        final String findById = "select* from m_cars where id=?";
        String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
        String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
        String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
        String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);
        try {
            Class.forName(driverName);
        }catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        List<Car>resultList = new ArrayList<>();
        Car car = null;
        ResultSet resultSet = null;
        try (Connection connection = DriverManager.getConnection(url, login, password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(findById);
            preparedStatement.setLong(1, carId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
               car = parseResultSet(resultSet);
            } else {
                throw new ResourceNotFoundException("Car with id" + carId + "not found");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            try{
                resultSet.close();
            }catch (SQLException throwables){
                System.out.println(throwables.getMessage());
            }
        }
          return car;
    }

    @Override
    public Car save(Car car) {
        final String insertQuery = "INSERT INTO public.m_cars (model, guarantee_expiration_date, price, dealer_id, user_id, number_of_cars\n" +
                "VALUES ( ?, ?, ?, ?, ?, ?)";
        String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
        String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
        String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
        String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);
        try {
            Class.forName(driverName);
        }catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        try (Connection connection = DriverManager.getConnection(url, login, password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
            PreparedStatement lastInsertId = connection.prepareStatement("select currval ('m_cars id_seq')as last_inset_id;");
            preparedStatement.setString(1, car.getModel());
            preparedStatement.setTimestamp(2, car.getGuarantee_expiration_date());
            preparedStatement.setInt(3,  car.getPrice());
            preparedStatement.setInt(4, car.getDealer_id());
            preparedStatement.setInt(5, car.getUser_id());
            preparedStatement.setInt(6, car.getNumber_of_cars());

            preparedStatement.executeUpdate();

            ResultSet set = lastInsertId.executeQuery();
            set.next();
            int insertedCarId = set.getInt("last_insert_id");
            return findOne(insertedCarId);
        }catch (SQLException e){
            throw new RuntimeException("Some issues in insert operation!", e);
        }
    }

    @Override
    public Car update(Car car) {
        final String updateQuery = "UPDATE public.m_cars set model=?, guarantee_expiration_date=?, price=?, dealer_id=?, user_id=?, number_of_cars=?)\n" +
                "WHERE id= ?";
        String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
        String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
        String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
        String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);
        try {
            Class.forName(driverName);
        }catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try (Connection connection = DriverManager.getConnection(url, login, password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(updateQuery);
            PreparedStatement lastInsertId = connection.prepareStatement("select currval ('m_cars id_seq')as last_inset_id;");
            preparedStatement.setString(1, car.getModel());
            preparedStatement.setTimestamp(2, car.getGuarantee_expiration_date());
            preparedStatement.setInt(3, car.getPrice());
            preparedStatement.setInt(4, car.getDealer_id());
            preparedStatement.setInt(5, car.getUser_id());
            preparedStatement.setInt(6, car.getNumber_of_cars());

            preparedStatement.setInt(9, car.getId());

            preparedStatement.executeUpdate();

        }catch (SQLException e1){
            throw new RuntimeException("Some issues in insert operation!", e1);
        }
        return  findOne(car.getId());
    }

    @Override
    public int delete(Car car) {
        final String deleteQuery = "DELETE public.m_cars set model=?, guarantee_expiration_date=?, price=?, dealer_id=?, user_id=?, number_of_cars=?)\n" +
                "WHERE id= ?";
        String driverName = config.getProperty(DatabaseConfiguration.DATABASE_DRIVER_NAME);
        String url = config.getProperty(DatabaseConfiguration.DATABASE_URL);
        String login = config.getProperty(DatabaseConfiguration.DATABASE_LOGIN);
        String password = config.getProperty(DatabaseConfiguration.DATABASE_PASSWORD);
        try {
            Class.forName(driverName);
        }catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try (Connection connection = DriverManager.getConnection(url, login, password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery);
            PreparedStatement lastInsertId = connection.prepareStatement("select currval ('m_cars id_seq')as last_inset_id;");
            preparedStatement.setString(1, car.getModel());
            preparedStatement.setTimestamp(2, car.getGuarantee_expiration_date());
            preparedStatement.setInt(3,  car.getPrice());
            preparedStatement.setInt(4, car.getDealer_id());
            preparedStatement.setInt(5, car.getUser_id());
            preparedStatement.setInt(6, car.getNumber_of_cars());

            preparedStatement.setInt(9,car.getId());

            preparedStatement.executeUpdate();


        }catch (SQLException e1){
            throw new RuntimeException("Some issues in insert operation!", e1);
        }
        return 0;
    }
    private Car parseResultSet(ResultSet resultSet) throws SQLException {
        Car car = new Car();
        car.setModel(resultSet.getString(CAR_MODEL));
        car.setGuarantee_expiration_date(resultSet.getTimestamp(CAR_GUARANTEE_EXPIRATION_DATE));
        car.setId(resultSet.getInt(CAR_ID));
        car.setPrice(resultSet.getInt(CAR_PRICE));
        car.setDealer_id(resultSet.getInt(CAR_DEALER_ID));
        car.setUser_id(resultSet.getInt(CAR_USER_ID));
        car.setNumber_of_cars(resultSet.getInt(CAR_NUMBER_OF_CARS));

        return car;
    }
    }



