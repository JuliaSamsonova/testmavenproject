package com.http.config;

//import com.http.aspect.LogAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.net.http.HttpClient;

@Configuration
public class ApplicationBeanConfiguration {
    @Bean
    public HttpClient httpClient() {
        return HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .build();
    }
//    @Bean("logAspect")
//    public LogAspect getLogAspect() {
//        return new LogAspect();

    @Bean
    public ViewResolver getViewResolver() {
        return new InternalResourceViewResolver("/WEB-INF/JSP/", ".jsp");
    }
}
