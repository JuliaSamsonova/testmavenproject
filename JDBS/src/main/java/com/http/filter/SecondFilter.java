package com.http.filter;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.http.HttpRequest;

public class SecondFilter implements Filter {
    private String encoding;

    /* throws IOException
     *throws ServletException*/

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        System.out.println("It's the second filter");
        if (StringUtils.isNotBlank(((HttpServletRequest) request).getHeader("Custom-Header"))) {
            System.out.println("Header was found");
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}












