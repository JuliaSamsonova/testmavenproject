package com.http.domain;

import java.sql.Timestamp;
import java.util.Objects;

public class Car {
    private int id;
    private String model;
    private Timestamp guarantee_expiration_date;
    private int price;
    private int dealer_id;
    private int user_id;
    private int number_of_cars;

    public Car() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Timestamp getGuarantee_expiration_date() {
        return guarantee_expiration_date;
    }

    public void setGuarantee_expiration_date(Timestamp guarantee_expiration_date) {
        this.guarantee_expiration_date = guarantee_expiration_date;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDealer_id() {
        return dealer_id;
    }

    public void setDealer_id(int dealer_id) {
        this.dealer_id = dealer_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getNumber_of_cars() {
        return number_of_cars;
    }

    public void setNumber_of_cars(int number_of_cars) {
        this.number_of_cars = number_of_cars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return id == car.id &&
                price == car.price &&
                dealer_id == car.dealer_id &&
                user_id == car.user_id &&
                number_of_cars == car.number_of_cars &&
                Objects.equals(model, car.model) &&
                Objects.equals(guarantee_expiration_date, car.guarantee_expiration_date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, model, guarantee_expiration_date, price, dealer_id, user_id, number_of_cars);
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", model='" + model + '\'' +
                ", guarantee_expiration_date=" + guarantee_expiration_date +
                ", price=" + price +
                ", dealer_id=" + dealer_id +
                ", user_id=" + user_id +
                ", number_of_cars=" + number_of_cars +
                '}';
    }
}
