package com.http.dao;

import com.http.domain.Car;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface CarDao {
    List<Car> findAll() throws SQLException;

    List<Car> search(String searchParam);

    Optional<Car> findById(int carId);

    Car findOne(int carId); //при вызове данного метода user должен быть (try-catch)

    Car save(Car car);

    Car update(Car car);

    int delete(Car car);
}
